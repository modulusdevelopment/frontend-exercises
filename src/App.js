import React from "react";
import "./App.css";

function App() {
  const datasets = [];
  const selected = [];
  const unselected = [];
  const apiKey = "";
  const apiUrl = `http://api.planetos.com/v1/datasets?apikey=${apiKey}`;

  return (
    <div className="App">
      <header className="App-header">
        <h1>DATAHUB DATASETS</h1>

        {!!datasets.length && (
          <div className="dataset-columns">
            <div className="available-datasets">
              <h2>Available Datasets: {datasets.length}</h2>

              <ul className="datasets">
                {datasets &&
                  datasets.map(dataset => (
                    <li className="dataset">
                      <input type="checkbox" name={dataset} id={dataset} />
                      <label for={dataset}>{dataset}</label>
                    </li>
                  ))}
              </ul>
            </div>
            <div className="selected-datasets">
              <h2>Selected Datasets: {selected.length}</h2>
              <ul className="datasets">
                {selected &&
                  selected.map(dataset => (
                    <li className="dataset">
                      <input type="checkbox" name={dataset} id={dataset} />
                      <label for={dataset}>{dataset}</label>
                    </li>
                  ))}
              </ul>
            </div>
            <div className="unselected-datasets">
              <h2>Unselected Datasets: {unselected.length}</h2>
              <ul className="datasets">
                {unselected &&
                  unselected.map(dataset => (
                    <li className="dataset">
                      <input type="checkbox" name={dataset} id={dataset} />
                      <label for={dataset}>{dataset}</label>
                    </li>
                  ))}
              </ul>
            </div>
          </div>
        )}
      </header>
    </div>
  );
}

export default App;
