# Datahub Dataset Selection

This exercise explores application state management.

Before you start, sign up for an account on Datahub: https://data.planetos.com/signup/.

After logging in, go to https://data.planetos.com/account/settings/ to access your API key.

The application is based on create-react-app and work has already been started in `src/App.js`.

There you'll find three columns for datasets: **available**, **selected** and **unselected**.

### Requirements

- The application should query the Datahub API for a list of datasets - a good opportunity to use that API key!

- By selecting an available dataset the selected dataset column should be poplulated.

- Similarly, unselected dataset column should reflect all datasets that are not selected.

- Clicking on a dataset in the selected column should change it's state to unselected.

- Clicking on a dataset in the unselected column should change it's state to selected.

- The time estimate for the task is 2 hours. Don't spend two days, for your own sake :)

### Bonus

1. Testing is welcome
2. Styling also welcome

> Good luck!
